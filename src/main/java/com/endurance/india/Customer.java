package com.endurance.india;

public class Customer {
    private Address address;

    public Customer()
    {
        System.out.println("Calling Empty Customer Constructor");
    }

    public Customer(Address customerAddress) {
        System.out.println("Calling Overloaded Customer Constructor");
        this.address = customerAddress;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    @Override
    public String toString()
    {
        return "Customer{" +
                "address=" + address +
                '}';
    }
}