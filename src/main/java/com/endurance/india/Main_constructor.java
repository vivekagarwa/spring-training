package com.endurance.india;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main_constructor
{

    public static void main(String[] args) {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("application-context-constructor.xml");

        Customer bean1 = ctx.getBean("customer1", Customer.class);
        System.out.println(bean1);

        Customer bean2 = ctx.getBean("customer2", Customer.class);
        System.out.println(bean2);

        Customer bean3 = ctx.getBean("customer3", Customer.class);
        System.out.println(bean3);

        ((ClassPathXmlApplicationContext) (ctx)).close();
    }

}