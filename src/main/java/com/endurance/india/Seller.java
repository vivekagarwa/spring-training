package com.endurance.india;

public class Seller
{
    private Address address;

    public Seller()
    {
        System.out.println("Calling Empty Seller Constructor");
    }

    public Seller(Address sellerAddress) {
        System.out.println("Calling Overloaded Seller Constructor");
        this.address = sellerAddress;
    }

    public void setSellerAddress(Address address) {
        this.address = address;
    }

    @Override
    public String toString()
    {
        return "Seller{" +
                "address=" + address +
                '}';
    }
}