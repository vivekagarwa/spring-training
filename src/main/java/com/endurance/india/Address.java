package com.endurance.india;

public class Address
{
    private String fulladdress;
    private int houseNumber;

    public Address()
    {
        System.out.println("Calling Empty Address Constructor");
    }

    public Address(String fulladdress, int houseNumber) {
        System.out.println("Calling overloaded Address Constructor");
        this.fulladdress = fulladdress;
        this.houseNumber = houseNumber;
    }

    public void setFulladdress(String fulladdress)
    {
        this.fulladdress = fulladdress;
    }

    public void setHouseNumber(int houseNumber)
    {
        this.houseNumber = houseNumber;
    }

    @Override
    public String toString()
    {
        return "Address{" +
                "fulladdress='" + fulladdress + '\'' +
                ", houseNumber=" + houseNumber +
                '}';
    }
}