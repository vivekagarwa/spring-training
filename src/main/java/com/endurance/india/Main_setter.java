package com.endurance.india;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main_setter
{

    public static void main(String[] args) {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("application-context-setter.xml");

        Customer bean1 = ctx.getBean(Customer.class);
        System.out.println(bean1);

        Seller bean2 = ctx.getBean(Seller.class);
        System.out.println(bean2);

        ((ClassPathXmlApplicationContext) (ctx)).close();
    }

}